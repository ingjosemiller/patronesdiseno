<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GoodAlien
 *
 * @author pabhoz
 */
class GoodAlien extends Alien implements IGoodAlien{
    
    public function __construct($nombre, $edad, $especie, $planeta) {
        parent::__construct($nombre, $edad, $especie, $planeta);
        $this->setMoral("Bueno");
    }
    public function interact() {
        return BadAlien::COMUNICACION." dice: Hola terricola, mi nombre es ".
                $this->getNombre().", vinimos en son de paz";
    }

    public function llamarACasa() {
        return "Llamando a Caaaaasaaaaa";
    }

    public function salvarPlaneta(\Planet $planeta) {
        $planeta->setEstado("a salvo");
        return $planeta->getEstado();
    }

}
